<?php
$conn = null;

function OpenCon()
{
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $db = "rm_activator";
    $conn = new mysqli($dbhost, $dbuser, $dbpass, $db) or die("Connect failed: %s\n" . $conn->error);    
    return $conn;
}

function CloseCon($conn)
{
    $conn->close();
}



function insert_activation_record($conn, $serial_number, $activation_record, $created_at, $updated_at)
{
    $sql = "INSERT INTO activations_records (serial_number, activation_record, created_at, updated_at) 
    VALUES ('" . $serial_number . "', '" . $activation_record . "', '" . $created_at . "', '" . $updated_at . "')";

    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

function get_activation_record($conn, $serial_number)
{

    $sql = "SELECT * FROM activations_records WHERE serial_number = '" . $serial_number . "' ";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {

            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            return $row['activation_record'];
        }
    } else {
        throw new Exception('Activations Records not found for Serial: ' . $serial_number, 1);
    }
}
