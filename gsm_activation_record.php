<?php

include __DIR__ . '/config.php';
$conn = OpenCon();


if (isset($_GET['serial'])) {
    $activation_record_uri = get_activation_record($conn, $_GET['serial']);
    //echo $activation_record_uri;
    $content = file_get_contents('./var/www/requests/' . $activation_record_uri); 
    echo $content;
}else {
    echo new Exception("Error Processing Request", 1);    
}
